import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Level;


//Test class
public class HelloLog {

    private static final Logger logger = LogManager.getLogger();

    public static void main(String[] args) {
        String userInput = "${jndi:http://localhost/AAAA/BBBB}";

        // passing user input into the logger, it is a log4j critical vuln
        //antoher line
        logger.info("Test2: "+userInput);

        // %m{nolookups} has no effect for the following line
        logger.printf(Level.INFO,"Test: %s", userInput);

        // %m{nolookups} has no effect for the following line
        logger.printf(Level.INFO,"Test: %s", userInput);
    }
}